//
//  CardGameAppDelegate.h
//  Matchismo
//
//  Created by Richard Lo on 9/26/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardGameAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
