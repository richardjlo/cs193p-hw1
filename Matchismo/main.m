//
//  main.m
//  Matchismo
//
//  Created by Richard Lo on 9/26/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CardGameAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CardGameAppDelegate class]));
    }
}
