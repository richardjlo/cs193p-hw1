//
//  PlayingCardDeck.h
//  Matchismo
//
//  Created by Richard Lo on 9/30/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import "Deck.h"

@interface PlayingCardDeck : Deck

@end
