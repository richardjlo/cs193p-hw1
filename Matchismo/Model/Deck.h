//
//  Deck.h
//  Matchismo
//
//  Created by Richard Lo on 9/30/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void)addCard:(Card *)card atTop:(BOOL)atTop;

- (void)addCard:(Card *)card;

- (Card *)drawRandomCard;



@end
