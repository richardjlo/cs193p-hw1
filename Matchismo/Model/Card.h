//
//  Card.h
//  Matchismo
//
//  Created by Richard Lo on 9/30/13.
//  Copyright (c) 2013 RLo Labs. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Card : NSObject

@property (strong, nonatomic) NSString *contents;

@property (nonatomic, getter=isChosen) BOOL chosen;
@property (nonatomic, getter=isMatched) BOOL matched;

- (int)match:(NSArray *)otherCards;

@end
